from config import config
from SPARQLWrapper import SPARQLWrapper, XML, JSON, N3, RDFXML
from rdflib import Graph, Literal, URIRef, RDFS, RDF, OWL, BNode
import pandas as pd
import json
import requests
from datetime import datetime
import pickle

class pipeline():
    def __init__(self):
        self.data = {}
        self.candidates = {'properties': {}, 'items': {}}
        self.statements = []

    def authenticate(self, url):
        
        """
        Authenticates against the wikibase API, returns a token and a Requests session object.
        Returns:    session (requests object), 
                    token (API token)
        """
        self.session = requests.Session()

        params = {
            "action": "query",
            "meta": "tokens",
            "type": "csrf",
            "format": "json"
        }

        R = self.session.get(url=url, params=params)

        self.token = R.json()['query']['tokens']['csrftoken']

        print('API authentication successful.')

    def createGraph(self, datatype, namespaces):
        """
        Takes a list of namespaces available on the web or locally, builds a single combined RDF graph in memory
        Return: RDFLib.Graph()
            
        """
        
        self.data[datatype] = Graph()
        for ns in namespaces:
            if isinstance(ns, str):
                print("loading {}".format(ns))
                try:
                    self.data[datatype].parse(ns)
                except Exception as e:
                    print(e)
            else:
                print("loading {}".format(ns[0]))
                try:
                    self.data[datatype].parse(ns[0], format=ns[1])
                except Exception as e:
                    print(e)

        print("{} graph created. contains {} triples.".format(datatype, len(self.data[datatype])))

    def createItemCandidates(self):
        """
        Iterates over the unique predicates in the data graph
        Identifies predicates from the lookup graph that relate to label and description (i.e. rfds:label / skos:definition)
            # different ontologies may use other predicates to define label/description (i.e. dc:title / rdfs:comment)
            # requires some exploration of the data beforehand to determine where we will find label/description
        Stores literals in a dictionary according to the wikibase import format
            # also stores "statements", which will later be turned into claims under each property
        Returns a dictionary object with ontology subject URIs (derived from predicates in the data) as keys
        
        lookupGraph -- an RDFLib graph object containing namespaces (including the core ontology) referenced in the data
        dataGraph -- an RDFLib graph object containing data for import into wikibase
        
        Returns: data (dict)
        """
        subjects = list(set([s for s in self.data['data'].subjects() if isinstance(s, URIRef) and str(s) not in self.wikibaseItems['alias'].values]))
        objects = list(set([o for o in self.data['data'].objects() if isinstance(o, URIRef) and str(o) not in self.wikibaseItems['alias'].values]))
    
        entityCandidates = list(set(subjects + objects))

        print("{} item candidates to be processed".format(len(entityCandidates)))
        for candidate in entityCandidates:        
            self.candidates['items'][candidate] = {'type': 'item', 'labels': {}, 'descriptions': {}, "aliases": {"en": {"language": "en", "value": str(candidate)}}, 'datatype': 'string'}
            for p, o in self.data['data'].predicate_objects(candidate):
                if p == URIRef("http://www.w3.org/2000/01/rdf-schema#label"):
                    # add labels and descriptions to this candidate
                    self._buildValues(o, 'labels', candidate)
                    self._buildValues(o, 'descriptions', candidate)        
            
            # if this subject had neither a label or a description, set the label
            if not bool(self.candidates['items'][candidate]['labels']) and not bool(self.candidates['items'][candidate]['descriptions']):
                self.candidates['items'][candidate]['labels']['en'] = {'language': 'en', 'value': str(candidate).split('/')[-1]}

            # if this subject has the exact same label and description (illegal in wikibase), remove the description
            if 'en' in self.candidates['items'][candidate]['labels'] and 'en' in self.candidates['items'][candidate]['descriptions']:
                if self.candidates['items'][candidate]['labels']['en']['value'] == self.candidates['items'][candidate]['descriptions']['en']['value']:
                    self.candidates['items'][candidate]['descriptions'] = {}
    
            if 'fr' in self.candidates['items'][candidate]['labels'] and 'fr' in self.candidates['items'][candidate]['descriptions']:
                if self.candidates['items'][candidate]['labels']['fr']['value'] == self.candidates['items'][candidate]['descriptions']['fr']['value']:
                    self.candidates['items'][candidate]['descriptions'] = {}

        print("{} item candidates transformed for load".format(len(self.candidates['items'])))

    def createPropertyCandidates(self):
        """
        Iterates over the unique predicates in the data graph
        Identifies predicates from the lookup graph that relate to label and description (i.e. rfds:label / skos:definition)
            # different ontologies may use other predicates to define label/description (i.e. dc:title / rdfs:comment)
            # requires some exploration of the data beforehand to determine where we will find label/description
        Stores literals in a dictionary according to the wikibase import format
            # also stores "statements", which will later be turned into claims under each property
        Returns a dictionary object with ontology subject URIs (derived from predicates in the data) as keys
        
        lookupGraph -- an RDFLib graph object containing namespaces (including the core ontology) referenced in the data
        dataGraph -- an RDFLib graph object containing data for import into wikibase
        
        Returns: data (dict)
        """
        predicates = list(set([p for p in self.data['data'].predicates() if str(p) not in self.wikibaseProperties['alias'].values]))
        print("{} property candidates to be processed".format(len(predicates)))
        
        for candidate in predicates:
            
            # detect the object types for this property. Not complete. But it'll help.
            objects = list(set([type(o) for o in self.data['data'].objects(None, candidate)]))
            if (len(objects) == 1) and (URIRef in objects):
                datatype = 'wikibase-item'
            elif (len(objects) == 1) and ((Literal in objects) or (BNode in objects)):
                datatype = 'string'
            else:
                datatype = 'wikibase-item'

            self.candidates['properties'][candidate] = {'type': 'property', 'datatype': datatype, 'labels': {}, 'descriptions': {}, "aliases": {"en": {"language": "en", "value": str(candidate)}}}
            for p, o in self.data['lookup'].predicate_objects(candidate):
                if p == URIRef("http://www.w3.org/2000/01/rdf-schema#label"):
                    self._buildValues(o, 'labels', candidate)
                elif (p == URIRef("http://www.w3.org/2004/02/skos/core#definition")) or (p == URIRef("http://www.w3.org/2000/01/rdf-schema#comment")):
                    self._buildValues(o, 'descriptions', candidate)
                # if this subject has the exact same label and description (illegal in wikibase), remove the description
            if 'en' in self.candidates['properties'][candidate]['labels'] and 'en' in self.candidates['properties'][candidate]['descriptions']:
                if self.candidates['properties'][candidate]['labels']['en']['value'] == self.candidates['properties'][candidate]['descriptions']['en']['value']:
                    self.candidates['properties'][candidate]['descriptions'] = {}
            if 'fr' in self.candidates['properties'][candidate]['labels'] and 'fr' in self.candidates['properties'][candidate]['descriptions']:
                if self.candidates['properties'][candidate]['labels']['fr']['value'] == self.candidates['properties'][candidate]['descriptions']['fr']['value']:
                    self.candidates['properties'][candidate]['descriptions'] = {}

        print("{} property candidates transformed for load".format(len(self.candidates['properties'])))

    def getEntities(self, entityTypes = ('properties', 'items')):
        
        """
        The wikibase API doesn't provide a good service for retrieving all items (if someone figures it out, let me know).
        The sparql endpoint can do it just as well, and probably faster.

        Takes a string ('properties' | 'items'), tuple, or list.

        Sets wikibaseProperties and wikibaseItems as dataframes
        """
        
        sparql = SPARQLWrapper(config.sparqlEndpoint)
        queryStrings = {
            'property': 
                """
                #All properties with descriptions and aliases and types
                SELECT ?Pnum ?property ?propertyType ?propertyLabel ?propertyDescription ?propertyAltLabel WHERE {
                ?property wikibase:propertyType ?propertyType .
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                BIND(REPLACE(STR(?property), "http://wikibase/entity/", "") AS ?Pnum)
                }
                ORDER BY ASC(xsd:integer(STRAFTER(STR(?property), 'P')))
                """,
            'item': 
                """
                SELECT ?subject ?alias
                WHERE {
                ?subject skos:altLabel ?alias
                FILTER(contains(STR(?subject), "http://wikibase/entity/Q"))
                }
                """
            }
        
        if 'properties' in entityTypes:
            sparql.setQuery(queryStrings['property'])
            sparql.setReturnFormat(JSON)
            results = sparql.query().convert()['results']['bindings']
            self.wikibaseProperties = pd.DataFrame(columns = ['PNum', 'alias'])
            self.wikibaseProperties = self.wikibaseProperties.append([{'PNum': str(row['Pnum']['value']), 'alias': str(row['propertyAltLabel']['value'])} for row in results])
            print('{} properties identified in wikibase and added to the pipeline.'.format(len(self.wikibaseProperties)))

        if 'items' in entityTypes:
            sparql.setQuery(queryStrings['item'])
            sparql.setReturnFormat(JSON)
            results = sparql.query().convert()['results']['bindings']       
            self.wikibaseItems = pd.DataFrame(columns = ['QNum', 'alias'])
            self.wikibaseItems = self.wikibaseItems.append([{'QNum': str(row['subject']['value']).replace('http://wikibase/entity/',''), 'alias': str(row['alias']['value'])} for row in results])
            print('{} items identified in wikibase and added to the pipeline.'.format(len(self.wikibaseItems)))

    def getStatements(self):
        
        """
        Iterates over existing wikibase items.
        Iterates over triples in the source data, adding them to 
        """
        print('Preparing statements.')

        for index, item in self.wikibaseItems.iterrows():
            for p, o in self.data['data'].predicate_objects(URIRef(item['alias'])):
                self.statements.append(self._buildStatement(item['QNum'], p, o))

        print('{} statements prepared for load.'.format(len(self.statements)))

    def loadEntities(self, entityType):
        
        """
        Iterates over a dictionary of property/item candidates.
        Pushes each one through the API into wikibase.
        
        --TOKEN: a csfr token acquired from wikibase upon authentication.
        --SESSION: the requests session
        --candidates: a dictionary of wikibase entity candidates prepared for ingest.
        
        """
        self.completedEntities = []
        e = 0
        s = 0

        print('loading entities.')
        for candidate in self.candidates[entityType]:  
            payload = {
                "action": "wbeditentity",
                "new": self.candidates[entityType][candidate]['type'],
                "format": "json",
                "token": self.token, 
                "data": json.dumps({
                    "labels": self.candidates[entityType][candidate]["labels"],
                    "descriptions": self.candidates[entityType][candidate]["descriptions"],
                    "aliases": self.candidates[entityType][candidate]["aliases"],
                    "datatype": self.candidates[entityType][candidate]["datatype"]})
            }

            r = self.session.post(config.url, data = payload )
            self.completedEntities.append(r.json())

            if "success" in r.json().keys():
                s = s + 1
            else:
                e = e + 1
        print("")
        print("{} successfuly loaded. {} failed.".format(s, e))
        
    def loadStatements(self):
        self.completedStatements = []
        e = 0
        s = 0
        print('loading statements.')
        for statement in self.statements:
            # some statements seem to have yielded empty dicts. Not sure went wrong. Need more error checking on statement creation.
            if isinstance(statement, dict):
                statement['action'] = "wbcreateclaim"
                statement["format"] = "json"
                statement["token"] = self.token
                
                r = self.session.post(config.url, data = statement )
                self.completedStatements.append(r.json())
                if "success" in r.json().keys():
                    s = s + 1
                else:
                    e = e + 1
        print("")
        print("{} successfuly loaded. {} failed.".format(s, e))

    def saveLog(self, logType):
        
        """
        Saves the most recent log file (documenting loads)
        """
        filename = 'logs/{}-{}.json'.format(logType, datetime.now().strftime("%d-%m-%Y %H:%M:%S"))
        with open(filename, 'w+') as outfile:
            if logType in ['properties', 'entities']:
               json.dump(self.completedEntities, outfile)
            elif logType == 'statements':
                json.dump(self.completedStatements, outfile)
        print('completed {} saved to {}'.format(logType, filename))        

    def _buildStatement(self, qnum, p, o):

        """
        Detects the correct claim type and fills the dictionary with corresponding data
        """
        try:
            pnum = str(self.wikibaseProperties[self.wikibaseProperties['alias'] == str(p)]['PNum'].values[0])
        
            if isinstance(o, URIRef) and str(o) in self.wikibaseItems['alias'].values:
                # the value is a uri, which has already been loaded as a wikibase item
                return {
                    "entity": qnum,
                    "snaktype": "value", 
                    "property": pnum, 
                    "value": json.dumps({
                                "entity-type": "item", 
                                "numeric-id": int(str(self.wikibaseItems[self.wikibaseItems['alias'] == str(o)]['QNum'].values[0]).replace('Q',''))
                            })
                }

            elif isinstance(o, BNode):
                # wikibase does not handle bnodes as subjects (which happen to exist in CWRC), but it does treat bnode objects as "somevalue" or "novalue". It may be hard to determine which is correct in a given instance.
                return {
                    "entity": qnum,
                    "snaktype": "somevalue", 
                    "property": pnum
                }

            else:
                return {
                        "entity": qnum,
                        "snaktype": "value", 
                        "property": pnum, 
                        "value": "\"{}\"".format(str(o)) 
                }
        except Exception as e:
            # print('qnum = {}, pnum = {}, error: {}',format(qnum, pnum, e))
            print(e)
            pass

    def _buildValues(self, o, vType, candidate):

        try:
            if hasattr(Literal(o), 'language'):        
                if o.language is None:
                    language = 'en'
                else:
                    language = o.language

                if o.value == '':
                    v = str(candidate)
                else:
                    v = o.value[:250]

                self.candidates['properties'][candidate][vType][language] = {"language": language, "value": v}
            else:
                self.candidates['properties'][candidate][vType]['en'] = {"language": 'en', "value": str(candidate)}
        except Exception as e:
            pass
 
if __name__ == "__main__":
    # initialize the pipeline

    startTime = datetime.now()
    print('started: {}'.format(startTime.strftime("%d-%m-%Y %H:%M:%S")))
    pipeline = pipeline()

    # retrieve existing wikibase properties/entities
    pipeline.getEntities()

    # add source rdf
    pipeline.createGraph('lookup', config.namespaces)
    pipeline.createGraph('data', config.dataNamespace)
    
    # authenticate against wikibase
    pipeline.authenticate(config.url)
    
    ### prepare and load properties
    pipeline.createPropertyCandidates()
    pipeline.loadEntities('properties')
    pipeline.saveLog('properties')

    # refresh existing wikibase properties index
    pipeline.getEntities('properties')

    ### prepare and load items
    pipeline.createItemCandidates()
    pipeline.loadEntities('items')
    pipeline.saveLog('items')

    # refresh existing wikibase items index
    pipeline.getEntities('items')
    
    # prepare and load individual statements for wikibase items
    pipeline.getStatements()
    with open('backup/pickledStatements', 'wb') as f:
        pickle.dump(pipeline.statements, f)
    pipeline.loadStatements()
    pipeline.saveLog('statements')

    endTime = datetime.now()
    print('ended: {}'.format( endTime.strftime("%d-%m-%Y %H:%M:%S") ))
    print('time to finish: {}'.format((endTime - startTime)))