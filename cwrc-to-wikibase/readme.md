## Importing RDF into Wikibase

<br>

### __Objective__:
* Import an RDF dataset into a fresh install of Wikibase, treating all RDF predicates as a 1:1 mapping to new wikibase properties, and all subject/objects as core entities. Statements are included, but qualifiers and references have not been.

### __Steps__:
1. Import properties
2. Import core items
3. Import statements/claims
4. Import qualifiers & references (not completed)

<br>

### __Dataset__:

Canadian Writing Research Collaboratory (CWRC) Biography Dataset
* 1,942,077 triples
* 35 external namespaces
* Stored locally (.ttl)
* Defined by CWRC Ontology (.ttl)

### Importing Properties

1. Build a local repository containing remote namespace data (lookupGraph). Build another repository for the data (dataGraph).
```    
from rdflib import Graph

namespaces = [
    # add url links to remote namespaces.
    # format urls as tuples, including the format as an additional arg.
    # i.e. ("http://dbpedia.org/data/.ttl", 'turtle')
]

lookupGraph = Graph()
for ns in namespaces:
    lookupGraph.parse(ns[0], format=ns[1])

dataGraph = Graph
dataGraph.parse("pathToData.ttl")
```

2. Fetch existing property data from wikibase to reconcile against to determine if properties already exist. Results will be empty for a fresh install. We include it here in case this process needs to be repeated, so duplicate entries can be avoided. We load the sparql results into a pandas dataframe to improve efficiency of lookups.

```
from SPARQLWrapper import SPARQLWrapper, JSON
import pandas as pd

sparql = SPARQLWrapper("http://wdqs.example.com/proxy/wdqs/bigdata/namespace/wdq/sparql")

sparql.setQuery("""
    SELECT ?Pnum ?property ?propertyType ?propertyLabel ?propertyDescription ?propertyAltLabel 
    WHERE {
    ?property wikibase:propertyType ?propertyType .
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    BIND(REPLACE(STR(?property), "http://wikibase/entity/", "") AS ?Pnum)
    }
    """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()['results']['bindings']
wikibaseProperties = pd.DataFrame(columns = ['PNum', 'alias'])
wikibaseProperties = wikibaseProperties.append([{'PNum': str(row['Pnum']['value']), 'alias': str(row['propertyAltLabel']['value'])} for row in results])
```

3. Authenticate against wikibase. Return the session object and an auth token.

```
import requests
session = requests.Session()

params = {
    "action": "query",
    "meta": "tokens",
    "type": "csrf",
    "format": "json"
}

R = session.get(url="www.example.com//w/api.php", params=params)

token = R.json()['query']['tokens']['csrftoken']

return session, token
    
```

4. Preparing candidates for load requires isolating a list of all predicates used in the dataset, and looking up those predicates in the namespace graph to extract a label (i.e. ```rfds:label```) and description (i.e. ```skos:definition```). In the current implementation, we also check the object datatypes (uri, bnode, literal) to determine property type for this particular predicate. This has led to improperly defined property types, and we should instead investigate how to use ```rdfs:range``` to infer the datatype. Assigning the apppropriate datatype is critical because a property can only be used to make statements when the object of the statement aligns with the datatype. 
        
```
candidates = {}

# get the predicates, except for those already in wikibase
predicates = list(set([p for p in dataGraph.predicates() if str(p) not in wikibaseProperties['alias'].values]))

for candidate in predicates:

    # detect the object types for this property (insufficient for datatyping (as implemented))
    objects = list(set([type(o) for o in dataGraph.objects(None, candidate)]))
    if (len(objects) == 1) and (URIRef in objects):
        datatype = 'wikibase-item'
    elif (len(objects) == 1) and ((Literal in objects) or (BNode in objects)):
        datatype = 'string'
    else:
        datatype = 'wikibase-item'

    candidates[candidate] = {
        'type': 'property', 
        'datatype': datatype, 
        'labels': {}, 
        'descriptions': {}, 
        "aliases": {
            "en": {"language": "en", "value": str(candidate)}
            }
        }
    
    # get data about this predicate from the lookup graph
    for p, o in lookupGraph.predicate_objects(candidate):
        if p == URIRef("http://www.w3.org/2000/01/rdf-schema#label"):
            candidates[candidate]['labels'][o.lang] = buildValues(o,candidate)
        elif (p == URIRef("http://www.w3.org/2004/02/skos/core#definition")) or (p == URIRef("http://www.w3.org/2000/01/rdf-schema#comment")):
            candidates[candidate]['descriptions'][o.lang] = buildValues(o, candidate)
    
    return candidates

def buildValues(o, candidate):

    if hasattr(Literal(o), 'language'):        
        if o.language is None:
            language = 'en'
        else:
            language = o.language

        if o.value == '':
            v = str(candidate)
        else:
            v = o.value[:250]

        return {"language": language, "value": v}
    else:
        return {"language": 'en', "value": str(candidate)}
```

5. Next, we load our property candidates into Wikibase.

```
completedEntities = []
for candidate in candidates:  
    payload = {
        "action": "wbeditentity",
        "new": candidates[candidate]['type'],
        "format": "json",
        "token": token, 
        "data": json.dumps({
            "labels": candidates[candidate]["labels"],
            "descriptions": candidates[candidate]["descriptions"],
            "aliases": candidates[candidate]["aliases"],
            "datatype": candidates[candidate]["datatype"]})
    }

    r = session.post(url = url, data = payload )
    completedEntities.append(r.json())
```

6. Build and load core entities. We re-use the property candidates code, with the one difference being the use of subjects/objects from the data graph in place of the predicates. 

```
subjects = list(set([s for s in dataGraph['data'].subjects() if isinstance(s, URIRef) and str(s) not in wikibaseItems['alias'].values]))
objects = list(set([o for o in dataGraph['data'].objects() if isinstance(o, URIRef) and str(o) not in wikibaseItems['alias'].values]))

entityCandidates = list(set(subjects + objects))

# Use code for generating property candidates / load 
# For the load, the data structure and API parameters remain the same.
```

7. Build and load statements.
