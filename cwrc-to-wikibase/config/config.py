# add any namespaces that appear in the data. If these error, try adding a format to the tuple.
namespaces = [
("https://www.w3.org/ns/activitystreams.jsonld"),
("http://id.loc.gov/ontologies/bibframe.rdf"),
("https://www.dublincore.org/specifications/bibo/bibo/bibo.rdf.xml", 'xml'),
("https://vocab.org/bio/schema.rdf", 'xml'),
("https://sparontologies.github.io/cito/current/cito.ttl", "turtle"),
("http://purl.org/spar/biro.ttl", 'turtle'),
("http://dbpedia.org/data/.ttl", 'turtle'),
("https://www.dublincore.org/specifications/dublin-core/dcmi-terms/dublin_core_terms.rdf"),
# skipping (too large) ("https://op.europa.eu/o/opportal-service/euvoc-download-handler?cellarURI=http%3A%2F%2Fpublications.europa.eu%2Fresource%2Fcellar%2Fd707f2d7-bcbb-11ea-811c-01aa75ed71a1.0001.03%2FDOC_1&fileName=eurovoc-skos-ap-act.rdf"),
("http://sparql.cwrc.ca/ontologies/genre.rdf"),
("http://www.geonames.org/ontology/ontology_v2.2.1.rdf"),
("http://vocab.getty.edu/ontology.rdf"),
("http://sparql.cwrc.ca/ontologies/ii.rdf"),
# can't locate the rdf file ("http://prismstandard.org/namespaces/1.2/basic.rdf"),
("http://www.w3.org/ns/prov.rdf"),
("https://schema.org/version/latest/schemaorg-current-http.rdf"),
("https://semanticweb.cs.vu.nl/2009/11/sem/sem.rdf", "xml"),    
("http://www.w3.org/TR/skos-reference/skos.rdf"),
("http://www.w3.org/2006/time.rdf"),
# can't locate the rdf file ("http://purl.org/vocab/vann"),
("https://lov.linkeddata.es/vocommons/voaf/v2.3/voaf_v2.3.rdf"),
("http://rdfs.org/ns/void.rdf",'xml'),
# parsing problems ("http://www.w3.org/XML/1998/namespace.xml", 'xml'),
# can't find a good rdf file ("http://www.w3.org/2001/XMLSchema.rdf"),
("https://www.w3.org/2000/01/rdf-schema"),
("https://www.w3.org/1999/02/22-rdf-syntax-ns"),
("https://www.w3.org/2002/07/owl"),
("https://semanticweb.cs.vu.nl/2009/11/sem/sem.rdf", "xml"),
("https://www.dublincore.org/specifications/dublin-core/dcmi-terms/dublin_core_terms.rdf"),
("http://xmlns.com/foaf/spec/index.rdf"),            
("https://creativecommons.org/schema.rdf"),
("http://id.loc.gov/vocabulary/relators.rdf"),
("http://www.w3.org/2003/06/sw-vocab-status/ns#"),
("http://www.w3.org/ns/prov.rdf"),
("https://www.w3.org/ns/oa.rdf"),
("http://www.w3.org/ns/org.rdf"),
("data/cwrc.ttl", "turtle")
]



# add the location of the data graph (i.e. data source), and it's RDFLib format
dataNamespace = [("data/cwrcData.ttl", "turtle")]

# set wikibase API / SPARQL endpoints
url = "https://wikibase.zacks.sandbox.lincsproject.ca/w/api.php"
sparqlEndpoint = "https://wdqs.zacks.sandbox.lincsproject.ca/proxy/wdqs/bigdata/namespace/wdq/sparql"